﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
    [SerializeField] private Button closeButton;
    [SerializeField] private Button hintButton;
    [SerializeField] private Button quitButton;
    
    [SerializeField] private GameObject menuParent;
    [SerializeField] private EndMenu endMenu;
    [SerializeField] private ItemController itemController;
    [SerializeField] private PlayerController playerController;

    private void Start()
    {
        endMenu.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        closeButton.onClick.AddListener(HandleMenuToggle);
        hintButton.onClick.AddListener(HandleHintClicked);
        quitButton.onClick.AddListener(HandleQuitClicked);
        itemController.GameOver += HandleGameOver;
    }

    private void OnDisable()
    {
        closeButton.onClick.RemoveListener(HandleMenuToggle);
        hintButton.onClick.RemoveListener(HandleHintClicked);
        quitButton.onClick.RemoveListener(HandleQuitClicked);
        itemController.GameOver -= HandleGameOver;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            HandleMenuToggle();
        }
    }

    private void HandleMenuToggle()
    {
        menuParent.gameObject.SetActive(!menuParent.activeInHierarchy);
        playerController.MenuOpen = menuParent.activeInHierarchy;
        Cursor.lockState = menuParent.activeInHierarchy ? CursorLockMode.None : CursorLockMode.Locked;
    }

    private void HandleHintClicked()
    {
        itemController.ShowHint();
        HandleMenuToggle();
    }

    private void HandleQuitClicked()
    {
        SceneManager.LoadScene(Constants.MenuScene);
    }

    private void HandleGameOver()
    {
        Cursor.lockState = CursorLockMode.None;
        endMenu.gameObject.SetActive(true);
        playerController.MenuOpen = true;
    }
}
