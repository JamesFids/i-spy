﻿using TMPro;
using UnityEngine;

public class LeaderView : MonoBehaviour
{
    [SerializeField] private TMP_Text playerNameField;
    [SerializeField] private TMP_Text playerScoreField;

    public void SetData(string name, string score)
    {
        playerNameField.text = name;
        playerScoreField.text = score;
    }
}
