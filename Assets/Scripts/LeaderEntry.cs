﻿using System;

[Serializable]
public class LeaderEntry
{
    public string Name;
    public int Score;
}

