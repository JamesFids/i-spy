﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const float CLAMP_ANGLE = 80f;
    
    [SerializeField] private float sensitivity;
    [SerializeField] private float panSensitivity;
    [SerializeField] private ItemController itemController;
    
    private Vector3 rotation = Vector3.zero;

    public bool MenuOpen = false;
    
    private void OnEnable()
    {
        rotation = transform.localRotation.eulerAngles;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    private void Update()
    {
        if (MenuOpen)
        {
            return;
        }

        CheckMouseControls();
        CheckKeyboardControls();
        itemController.HighlightItems(transform.position);
    }

    private void CheckMouseControls()
    {
        rotation.y += Input.GetAxis("Mouse X") * panSensitivity * Time.deltaTime;
        rotation.x += -Input.GetAxis("Mouse Y") * panSensitivity * Time.deltaTime;
        rotation.x = Mathf.Clamp(rotation.x, -CLAMP_ANGLE, CLAMP_ANGLE);
        transform.rotation = Quaternion.Euler(rotation.x, rotation.y, 0.0f);

        if (Input.GetMouseButtonDown(0))
        {
            itemController.SelectItem();
        }
    }

    private void CheckKeyboardControls()
    {
        if (!Input.anyKey && !Input.anyKeyDown)
        {
            return;
        }

        var delta = new Vector3
        (
            ToInt(KeyCode.D) - ToInt(KeyCode.A),
            0,
            ToInt(KeyCode.W) - ToInt(KeyCode.S)
        );

        if (delta.magnitude < Mathf.Epsilon)
        {
            return;
        }

        delta = delta.normalized * (sensitivity * Time.deltaTime);
        delta = delta.z * transform.forward + delta.x * transform.right;
        transform.position += new Vector3(delta.x, 0, delta.z);
    }

    private int ToInt(KeyCode keyCode)
    {
        return Input.GetKey(keyCode) ? 1 : 0;
    }
}
