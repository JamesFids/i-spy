﻿using cakeslice;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public Outline Outline;
    public string Hint;
}
