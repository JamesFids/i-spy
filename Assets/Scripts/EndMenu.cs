﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndMenu : MonoBehaviour
{
    [SerializeField] private Button replayButton;
    [SerializeField] private Button quitButton;
    
    private void OnEnable()
    {
        replayButton.onClick.AddListener(HandleReplayClicked);
        quitButton.onClick.AddListener(HandleQuitClicked);
    }

    private void OnDisable()
    {
        replayButton.onClick.RemoveListener(HandleReplayClicked);
        quitButton.onClick.RemoveListener(HandleQuitClicked);
    }

    private void HandleReplayClicked()
    {
        SceneManager.LoadScene(Constants.MainScene);
    }
    
    private void HandleQuitClicked()
    {
        SceneManager.LoadScene(Constants.MenuScene);
    }
}
