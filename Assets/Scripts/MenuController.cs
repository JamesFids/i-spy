﻿using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button leaderboardButton;
    [SerializeField] private Button leaderboardExitButton;
    [SerializeField] private Button quitButton;
    
    [SerializeField] private GameObject leaderboardParent;
    [SerializeField] private GameObject leaderboardEntryPrefab;
    [SerializeField] private Transform leaderboardViewport;
    [SerializeField] private TMP_InputField nameInputField;

    private void Start()
    {
        //leaderboard loading

        if (PlayerPrefs.HasKey(Constants.PlayerName))
        {
            nameInputField.text = PlayerPrefs.GetString(Constants.PlayerName);
        }

        var json = string.Empty;
        if (File.Exists(Constants.LeaderboardFile))
        {
            json = File.ReadAllText(Constants.LeaderboardFile);
            var leaders = JsonUtility.FromJson<LeaderData>(json);
            foreach (var leader in leaders.Leaders)
            {
                var prefab = Instantiate(leaderboardEntryPrefab).GetComponent<LeaderView>();
                prefab.transform.SetParent(leaderboardViewport);
                prefab.SetData(leader.Name, leader.Score.ToString());
            }
        }
        else
        {
            json = JsonUtility.ToJson(new LeaderData());
            File.WriteAllText(Constants.LeaderboardFile, json);
        }
        
        PlayerPrefs.SetString(Constants.Leaders, json);
    }

    private void OnEnable()
    {
        playButton.onClick.AddListener(HandlePlayClicked);
        leaderboardButton.onClick.AddListener(HandleLeaderboardClicked);
        leaderboardExitButton.onClick.AddListener(HandleLeaderboardClicked);
        quitButton.onClick.AddListener(HandleQuitClicked);
    }

    private void OnDisable()
    {
        playButton.onClick.RemoveListener(HandlePlayClicked);
        leaderboardButton.onClick.RemoveListener(HandleLeaderboardClicked);
        leaderboardExitButton.onClick.RemoveListener(HandleLeaderboardClicked);
        quitButton.onClick.RemoveListener(HandleQuitClicked);
    }

    private void HandlePlayClicked()
    {
        PlayerPrefs.SetString(Constants.PlayerName, nameInputField.text);
        SceneManager.LoadScene(Constants.MainScene);
    }

    private void HandleLeaderboardClicked()
    {
        leaderboardParent.SetActive(!leaderboardParent.activeInHierarchy);
    }

    private void HandleQuitClicked()
    {
        Application.Quit();
    }
}
