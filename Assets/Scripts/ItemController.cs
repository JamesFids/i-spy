﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemController : MonoBehaviour
{
    public Action GameOver;
    
    [SerializeField] private List<Interactable> items;
    [SerializeField] private float interactionDistance;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private TMP_Text playerNameField;
    [SerializeField] private TMP_Text playerScoreField;
    [SerializeField] private TMP_Text playerLivesField;
    [SerializeField] private TMP_Text chosenLetterField;
    [SerializeField] private TMP_Text highlightedItemField;
    [SerializeField] private TMP_Text hintField;
    [SerializeField] private GameObject hintParentField;

    private readonly Vector2 center = new Vector2(Screen.width / 2f, Screen.height / 2f);

    private Interactable highlightedItem;
    private Interactable chosenItem;
    private int score = 0;
    private int lives = 3;
    
    public void HighlightItems(Vector3 playerPosition)
    {
        highlightedItem = default;
        highlightedItemField.text = string.Empty;
        
        foreach (var item in items)
        {
            item.Outline.eraseRenderer = true;
            var distance = (item.transform.position - playerPosition).magnitude;
            if (distance > interactionDistance)
            {
                continue;
            }
            
            var ray = mainCamera.ScreenPointToRay(center);
            if (Physics.Raycast(ray, out var hit) && hit.transform == item.transform)
            {
                item.Outline.eraseRenderer = false;
                highlightedItem = item;
                highlightedItemField.text = item.name;
            }
        }
    }

    public void SelectItem()
    {
        if (highlightedItem == default)
        {
            return;
        }

        if (highlightedItem != chosenItem)
        {
            lives--;
            playerLivesField.text = lives.ToString();
            if (lives != 0)
            {
                return;
            }

            CheckLeader();
            hintParentField.SetActive(false);
            GameOver?.Invoke();

            return;
        }
        
        score++;
        playerScoreField.text = score.ToString();
        ChooseItem();
    }

    public void ShowHint()
    {
        hintParentField.SetActive(true);
    }

    private void Start()
    {
        playerLivesField.text = lives.ToString();
        playerNameField.text = PlayerPrefs.GetString(Constants.PlayerName);
        playerScoreField.text = "0";
        ChooseItem();
    }

    private void ChooseItem()
    {
        hintParentField.SetActive(false);
        chosenItem = items[Random.Range(0, items.Count - 1)];
        chosenLetterField.text = chosenItem.name.ToCharArray()[0].ToString();
        hintField.text = chosenItem.Hint;
    }

    private void CheckLeader()
    {
        //No empty player names for leaderboard
        if (playerNameField.text.Trim() == string.Empty)
        {
            return;
        }

        var entry = new LeaderEntry
        {
            Name = playerNameField.text.Trim(),
            Score = score
        };
        
        var json = PlayerPrefs.GetString(Constants.Leaders);
        var leaders = JsonUtility.FromJson<LeaderData>(json).Leaders;

        leaders.Add(entry);
        leaders = leaders.OrderByDescending(x => x.Score).Take(10).ToList();
        json = JsonUtility.ToJson(new LeaderData {Leaders = leaders});
        File.WriteAllText(Constants.LeaderboardFile, json);
    }
}
