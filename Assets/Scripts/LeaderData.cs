﻿using System;
using System.Collections.Generic;

[Serializable]
public class LeaderData
{
    public List<LeaderEntry> Leaders = new List<LeaderEntry>();
}
