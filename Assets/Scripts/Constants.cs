﻿
using System.IO;
using UnityEngine;

public static class Constants
{
    public static readonly string LeaderboardFile = Application.persistentDataPath + Path.AltDirectorySeparatorChar + "Leaderboard.txt";
   
    public const string MainScene = "MainScene";
    public const string MenuScene = "MenuScene";
    
    public const string PlayerName = "PlayerName";
    public const string Leaders = "Leaders";
}
